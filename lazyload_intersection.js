let observer;

function LazyLoad( element, callback )
{
	if( !( 'IntersectionObserver' in window ) )
	{
		callback();
		return;
	}

	if( !observer )
	{
		observer = new window.IntersectionObserver( entries =>
		{
			entries.forEach( entry =>
			{
				if( entry.isIntersecting )
				{
					observer.unobserve( entry.target );
					entry.target.dispatchEvent( new Event( 'lazyload' ) );
				}
			} );
		} );
	}

	observer.observe( element );
	element.addEventListener( 'lazyload', callback, { once: true } );
}

LazyLoad( document.getElementById( 'something' ), function()
{
	// `this` is the element
	console.log( this );
} );
